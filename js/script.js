window.onload = function() {
  setColorOverlaySize();
};

$(function() {
  $(window).resize(function() {
    setColorOverlaySize();
  });
  setScrollLinks();
});

function setColorOverlaySize() {

  $('.vizualization').each(function(i, elem) {
    var $img = $(elem).find('img');
    var $colorOverlay = $(elem).find('.color-overlay');

    $colorOverlay.css({
      height: $img.css('height'),
      marginTop: $img.css('margin-top')
    });
  });

  $('#map-holder').each(function(i, elem) {
    var $img = $(elem).find('img');
    var $colorOverlay = $(elem).find('.color-overlay');

    $colorOverlay.css({
      height: $img.css('height'),
      width: $('#map-holder').css('width'),
      marginTop: $img.css('margin-top')
    });
  });
}

function setScrollLinks() {

	$('.scroll-link').on('click', function() {
		var selector = $(this).data('scrollto');
		var duration = 400;
		var add = ( navigator.userAgent.search("Firefox") > -1 ) ? 1 : 0;

		if( $(selector).length > 0 )
			jQuery("html, body").animate({ scrollTop: jQuery(selector).offset().top - add }, duration);

		return false;
	});
}
